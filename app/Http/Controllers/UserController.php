<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        return view('users')
        ->with('name','Arnon Jutakarn')
        ->with('title', 'Laravel Framework');
    }
    function user(){

        $arr = [
            'id' => 1,
            'title' => 'Welcome to Users',
            'firstname' => 'Singpanpeth',
            'lastname' => 'Sribut',
            'skill' => [
                'th' => 80,
                'en' => 30
            ]
        ];
        print_r($arr);
        return view('users')->with('obj',$arr);
    }
}
