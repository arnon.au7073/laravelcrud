<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('users');
// });
// Route::get('/', 'UserController@user');
Route::get('/', function () {
    return view('users');
});
Route::get('/list', function () {
    return view('list');
});
Route::get('/add', function () {
    return view('add');
});
Route::get('/index', 'PeopleController@index');
Route::resource('people','PeopleController');
// Route::prefix('product')->group(function () {
//     Route::get('{id}','UserController@show');
//     Route::post('','UserController@store');
//     Route::put('{id}','UserController@update');
//     Route::delete('{id}','UserController@destroy');
// });
// Route::get('/profile/{id?}', function($id){
//     return 'Hello : ' . $id;
// })->where('id','[0-9]+');
// Route::get('/profile', function($id) {
//     return 'Hello : ' . $id;
// })->name('p');


    
