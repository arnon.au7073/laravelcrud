@extends('layout.master')
@section('title', 'Edit')
@section('css')
        @parent
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">   
@endsection

@section('content')
<div class="container">
    <form action="{{ url('people',[$people->id])}}" method="POST">
        @csrf
        @method('put')
        <h1>Edit People</h1>
        <div class="form-group" >
            <label>Firstname</label>
            <input type="text" name="fname" value="{{$people->fname}}" class="form-control">
        </div>
        <div class="form-group" >
            <label>Lastname</label>
            <input type="text" name="lname" value="{{$people->lname}}" class="form-control">
        </div> 
        <div class="form-group" >
            <label>Age</label>
            <input type="text" name="age" value="{{$people->age}}" class="form-control">
        </div>
        <button type="submit" class="btn btn-success">save</button>
        @if($errors->any())
        <div class="alert alert-danger">
            <ul> 
                @foreach ($errors->all() as $er)
                    <li>{{$er}}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </form>
</div>
@endsection
