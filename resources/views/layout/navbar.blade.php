<link rel="stylesheet" href="{{asset('css/main.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<nav class="navbar navbar-expand-sm navbar-light bg-primary navbar-dark justify-content-between">
    <ul class="navbar-nav">
            <li class="nav-item active">
            <a class="nav-link" href="#">Active</a>
        </li>
            <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
        </li>
            <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
        </li>
                <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
        </li>
    </ul>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Login</button>
</nav>
  <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="container">
            <h2>Form Validation</h2>
            <p>In this example, we use <code>.was-validated</code> to indicate what's missing before submitting the form:</p>
            <form action="/action_page.php" class="was-validated">
              <div class="form-group">
                <label for="uname">Username:</label>
                <input type="text" class="form-control" id="uname" placeholder="Enter username" name="uname" required>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
              </div>
              <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd" required>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
              </div>
              <div class="form-group form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" name="remember" required> I agree on blabla.
                  <div class="valid-feedback">Valid.</div>
                  <div class="invalid-feedback">Check this checkbox to continue.</div>
                </label>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
      </div>
    </div>
  </div>