@extends('layout.master')
@section('title','list')
@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
@endsection
@section('content')
<div class="container">
    @if(Session::has('message'))
    <div>
        {{Session::get('message')}}
    </div>
    @endif
    <h1 class="main-green">List Page</h1>
    <a href="{{ url('people/create') }}">
        <button type="button" class="btn btn-block btn-success">Create User</button>
    </a>
    <table class="table table-dark table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>fname</th>
                <th>lname</th>
                <th>age</th>
                <th>Created_at</th>
                <th>Updated_at</th>
                <th>Action</th>
            </tr>
        </thead>
        @foreach ($preple as $item)
        <tbody>
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->fname}}</td>
                <td>{{$item->lname}}</td>
                <td>{{$item->age}}</td>
                <td>{{$item->created_at}}</td>
                <td>{{$item->updated_at}}</td>
                <td>
                    <div class="form-inline">
                        <a href="{{ url('people/'.$item->id .'/edit') }}">
                            <button type="button" class="btn btn-primary">EDIT </button>
                        </a>
                        <form action="{{ url('people',$item->id ) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">DELETE</button>
                        </form>
                    </div>
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>
</div>
@endsection

@section('js')
    <script>
    </script>
@endsection