@extends('layout.master')
@section('title', 'Add')
@section('css')
        @parent
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">   
@endsection

@section('content')
<div class="container">
    <form action="{{ url('people') }}" method="POST">
        @csrf
        <div class="form-group" >
            <label>Firstname</label>
            <input type="text" name="fname" class="form-control">
        </div>
        <div class="form-group" >
                <label>Lastname</label>
                <input type="text" name="lname" class="form-control">
        </div> 
        <div class="form-group" >
                <label>Age</label>
                <input type="text" name="age" class="form-control">
        </div>
        <button type="submit" class="btn btn-success">save</button>
        @if($errors->any())
        <div class="alert alert-danger">
            <ul> 
                @foreach ($errors->all() as $er)
                    <li>{{$er}}</li>  
                @endforeach
            </ul>
        </div>
        @endif
    </form>
</div>
@endsection
